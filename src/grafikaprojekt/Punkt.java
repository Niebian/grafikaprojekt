/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafikaprojekt;

/**
 * Punkt opisywany przez 3 współrzędne oraz numer ID
 *
 * @author Grzechu
 */
class Punkt {

    private int id;
    private double x;
    private double y;
    private double z;

    /**
     * Konstruktor widocznego punktu opisywanego przez 3 współrzędne
     *
     * @param id
     * @param x
     * @param y
     * @param z
     */
    public Punkt(int id, double x, double y, double z) {
        this.id = id;
        this.x = x;
        this.y = -y;
        this.z = z;
    }

    /**
     * Konstruktor ukrytego punktu opisywanego przez 3 współrzędne
     *
     * @param x
     * @param y
     * @param z
     */
    public Punkt(double x, double y, double z) {
        this.x = x;
        this.y = -y;
        this.z = z;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public void translacja(double x, double y, double z) {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    public void obrotX(double X) {
        double yP = y;
        double zP = z;
        y = yP * Math.cos(Math.PI * (X / 180)) - zP * Math.sin(Math.PI * (X / 180));
        z = zP * Math.cos(Math.PI * (X / 180)) + yP * Math.sin(Math.PI * (X / 180));
    }

    public void obrotY(double Y) {
        double xP = x;
        double zP = z;
        x = xP * Math.cos(Math.PI * (Y / 180)) + zP * Math.sin(Math.PI * (Y / 180));
        z = zP * Math.cos(Math.PI * (Y / 180)) - xP * Math.sin(Math.PI * (Y / 180));
    }

    public void rzutuj(double d) {
        x = (x * d) / (z + d);
        y = (y * d) / (z + d);
        z = 0;
    }

    @Override
    public String toString() {
        return "\n\tid=" + id + "\tx=" + x + "\ty=" + y + "\tz=" + z;
    }
}
