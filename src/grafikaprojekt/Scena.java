/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafikaprojekt;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Grzechu
 */
public class Scena {

    private static Scena instance;
    private ArrayList<Punkt> punkty;
    private ArrayList<Odcinek> odcinki;
    private ArrayList<Powierzchnia> powierzchnie;
    private Punkt kamera;
    private double zoom;

    public static Scena getInstance() {
        if (instance == null) {
            instance = new Scena();
        }
        return instance;
    }

    public Scena() {
        punkty = new ArrayList<>();
        odcinki = new ArrayList<>();
        powierzchnie = new ArrayList<>();
        kamera = new Punkt(20, 6, -50);
        zoom = 1;
    }

    public ArrayList<Odcinek> getOdcinki() {
        return odcinki;
    }

    public void importPunkty(ArrayList<String[]> lista) {
        for (String[] punkt : lista) {
            punkty.add(new Punkt(Integer.parseInt(punkt[0]), Integer.parseInt(punkt[1]),
                    Integer.parseInt(punkt[2]), Integer.parseInt(punkt[3])));
        }
    }

    public void importOdcinki(ArrayList<String[]> lista) {
        for (String[] odcinek : lista) {
            odcinki.add(new Odcinek(Integer.parseInt(odcinek[0]), Integer.parseInt(odcinek[1])));
        }
    }

    public void importPowierzchnie(ArrayList<String[]> lista) {
        int i = 0;
        for (String[] pow : lista) {
            Powierzchnia powierzchnia = new Powierzchnia(Integer.parseInt(pow[0]),
                    Integer.parseInt(pow[1]), Integer.parseInt(pow[2]));
            powierzchnia.setBokA(obliczOdleglosc(punkty.get(powierzchnia.getA()), punkty.get(powierzchnia.getB())));
            powierzchnia.setBokB(obliczOdleglosc(punkty.get(powierzchnia.getB()), punkty.get(powierzchnia.getC())));
            powierzchnia.setBokC(obliczOdleglosc(punkty.get(powierzchnia.getA()), punkty.get(powierzchnia.getC())));

            if (i < 12) {
                powierzchnia.setKolor(Color.YELLOW);
            } else if (i < 24) {
                powierzchnia.setKolor(Color.GREEN);
            } else if (i < 36) {
                powierzchnia.setKolor(Color.RED);
            } else if (i < 48) {
                powierzchnia.setKolor(Color.PINK);
            }
            i++;
            
            powierzchnie.add(powierzchnia);
        }
    }

    public boolean translacja(double x, double y, double z) {
        /*for (Punkt p : punkty) {
            p.translacja(-x, -y, -z);
        }*/
        if (kamera.getZ() >= 0 && z > 0) {
            return false;
        } else {
            kamera.translacja(x, y, z);
            return true;
        }
    }

    public void obrot(double x, double y) {
        for (Punkt p : punkty) {
            p.translacja(-kamera.getX(), -kamera.getY(), -kamera.getZ());
            if (x == 0) {
                p.obrotY(y);
            } else {
                p.obrotX(x);
            }
            p.translacja(kamera.getX(), kamera.getY(), kamera.getZ());
        }
    }

    public boolean zoom(double z) {
        if (this.zoom <= 0.19 && z < 0.0) {
            return false;
        } else {
            this.zoom += z;
            return true;
        }
    }

    public ArrayList<Punkt> getPunktyRzut() {
        ArrayList<Punkt> listaPunktyRzut = new ArrayList<>();

        for (Punkt p : punkty) {
            Punkt punktRzutowany = new Punkt(p.getId(), p.getX() - kamera.getX(),
                    p.getY() - kamera.getY(), p.getZ() - kamera.getZ() - zoom);
            punktRzutowany.rzutuj(zoom);
            listaPunktyRzut.add(punktRzutowany);
        }

        return listaPunktyRzut;
    }

    @Override
    public String toString() {
        return "\nScena:" + "\n\nPunkty=" + punkty + "\nKamera=" + kamera + " zoom=" + zoom + "\nPowierzchnie:" + powierzchnie;// + "\n\nOdcinki=" + odcinki;
    }

    public ArrayList<Powierzchnia> getPowierzchnie() {
        przeliczPowierzchnie();
        Collections.sort(powierzchnie);
        return powierzchnie;
    }

    public void przeliczPowierzchnie() {
        for (Powierzchnia pow : powierzchnie) {
            double x = (punkty.get(pow.getA()).getX()
                    + punkty.get(pow.getB()).getX()
                    + punkty.get(pow.getC()).getX()) / 3;
            double y = (punkty.get(pow.getA()).getY()
                    + punkty.get(pow.getB()).getY()
                    + punkty.get(pow.getC()).getY()) / 3;
            double z = (punkty.get(pow.getA()).getZ()
                    + punkty.get(pow.getB()).getZ()
                    + punkty.get(pow.getC()).getZ()) / 3;
            Punkt srCiez = new Punkt(x, y, z);
            pow.setSrCiez(srCiez);

            pow.setOdlegloscOdSrodka(obliczOdleglosc(kamera, pow.getSrCiez()));
        }
    }

    private double obliczOdleglosc(Punkt A, Punkt B) {
        return Math.sqrt(Math.pow((A.getX() - B.getX()), 2)
                + Math.pow((A.getY() - B.getY()), 2)
                + Math.pow((A.getZ() - B.getZ()), 2));
    }
}
