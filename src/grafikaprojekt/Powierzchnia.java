/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafikaprojekt;

import java.awt.Color;

/**
 *
 * @author Grzechu
 */
public class Powierzchnia implements Comparable<Powierzchnia> {

    private final int a;
    private final int b;
    private final int c;
    private Punkt srCiez;
    private Color kolor;
    private double l;
    private double bokA;
    private double bokB;
    private double bokC;

    public Powierzchnia(int A, int B, int C) {
        this.a = A;
        this.b = B;
        this.c = C;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public Punkt getSrCiez() {
        return srCiez;
    }

    public void setSrCiez(Punkt srCiez) {
        this.srCiez = srCiez;
    }

    public Color getKolor() {
        return kolor;
    }

    public void setKolor(Color kolor) {
        this.kolor = kolor;
    }

    public double getOdlegloscOdSrodka() {
        return l;
    }

    public void setOdlegloscOdSrodka(double l) {
        this.l = l;
    }

    @Override
    public int compareTo(Powierzchnia t) {
        return (int) (t.getOdlegloscOdSrodka() - this.getOdlegloscOdSrodka());
    }

    @Override
    public String toString() {
        return "\n\ta=" + a + "\tb=" + b + "\tc=" + c + "\tkolor=" + kolor + ", l=" + l;
    }

    public double getBokA() {
        return bokA;
    }

    public void setBokA(double bokA) {
        this.bokA = bokA;
    }

    public double getBokB() {
        return bokB;
    }

    public void setBokB(double bokB) {
        this.bokB = bokB;
    }

    public double getBokC() {
        return bokC;
    }

    public void setBokC(double bokC) {
        this.bokC = bokC;
    }
}
